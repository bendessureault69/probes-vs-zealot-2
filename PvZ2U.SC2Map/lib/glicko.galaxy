const fixed MAX_OTHER_PLAYERS = 1;

const fixed DEFAULT = -1;

const fixed POINTS_WIN = 1;
const fixed POINTS_DRAW = 0.5;
const fixed POINTS_LOSE = 0;

const fixed MU = 1000;
const fixed PHI = 100;
const fixed SIGMA = 0.06;
const fixed TAU = 1.0;
const fixed EPSILON = 0.000001;
fixed Q;
const fixed Q_SQUARED = 0.33136863190489995;
const fixed MULTIPLIER = 1000;

struct gs_Rating{
    fixed mu;
    fixed phi;
    fixed sigma;
};

struct gs_Glicko2{
    fixed mu;
    fixed phi;
    fixed sigma;
    fixed tau;
    fixed epsilon;
};

struct gs_Series{
    gs_Rating[1] ratings;
    fixed[1] won;
};

gs_Glicko2 env;
gs_Series series;

gs_Rating rating1;
gs_Rating rating1_final;

void debugF(fixed f){
    UIDisplayMessage(PlayerGroupAll(), 7, FixedToText(f, 20));
}

//@Class
void Glicko2(structref<gs_Glicko2> target, fixed lp_mu, fixed lp_phi, fixed lp_sigma, fixed lp_tau, fixed lp_epsilon){
    fixed temp_mu = lp_mu;
    fixed temp_phi = lp_phi;
    fixed temp_sigma = lp_sigma;
    fixed temp_tau = lp_tau;
    fixed temp_epsilon = lp_epsilon;

    if(temp_mu == DEFAULT){ temp_mu = MU; }
    if(temp_phi == DEFAULT){ temp_phi = PHI; }
    if(temp_sigma == DEFAULT){ temp_sigma = SIGMA; }
    if(temp_tau == DEFAULT){ temp_tau = TAU; }
    if(temp_epsilon == DEFAULT){ temp_epsilon = MU; }

    target.mu = temp_mu;
    target.phi = temp_phi;
    target.sigma = temp_sigma;
    target.tau = temp_tau;
    target.epsilon = temp_epsilon;
}

// @Class : @Glicko2
void Rating(structref<gs_Glicko2> self, structref<gs_Rating> target, fixed lp_mu, fixed lp_phi, fixed lp_sigma){
    fixed temp_mu = lp_mu;
    fixed temp_phi = lp_phi;
    fixed temp_sigma = lp_sigma;
    if(temp_mu == DEFAULT){temp_mu = self.mu;}
    if(temp_phi == DEFAULT){temp_phi = self.phi;}
    if(temp_sigma == DEFAULT){temp_sigma = self.sigma;}
    target.mu = temp_mu;
    target.phi = temp_phi;
    target.sigma = temp_sigma;
    return;
}

// @Glicko2
void gf_ScaleDown(structref<gs_Glicko2> self, structref<gs_Rating> target, structref<gs_Rating> rating, fixed temp_ratio){
    fixed ratio = temp_ratio;
    fixed mu;
    fixed phi;

    if(ratio == DEFAULT){
        ratio = 173.7178;
    }

    mu = (rating.mu - self.mu) / ratio;
    phi = rating.phi / ratio;

   Rating(self, target, mu, phi, rating.sigma);
   return;
}

// @Glicko2
void gf_ScaleUp(structref<gs_Glicko2> self, structref<gs_Rating> target, structref<gs_Rating> rating, fixed temp_ratio){
    fixed ratio = temp_ratio;
    fixed mu;
    fixed phi;

    if(ratio == DEFAULT){
        ratio = 173.7178;
    }

    mu = rating.mu * ratio + self.mu;
    phi = rating.phi * ratio;

   Rating(self, target, mu, phi, rating.sigma);
   return;
}

// @Glicko2
fixed gf_ReduceImpact(structref<gs_Glicko2> self, structref<gs_Rating> rating){
    return 1 / SquareRoot(1 + (3 * Pow(rating.phi,2)) / Pow(3.1415,2));
}

// @Glicko2
fixed gf_ExpectScore(structref<gs_Glicko2> self, structref<gs_Rating> rating, structref<gs_Rating> other_rating, fixed impact){
    return 1. / (1 + Pow(2.718281, (-impact * (rating.mu - other_rating.mu))));
}

fixed f(fixed x, fixed difference2, fixed variance, fixed alpha, fixed tau, fixed phi){
    fixed tmp = Pow(phi,2) + variance + Pow(2.718281,x);
    fixed a = Pow(2.718281,x) * (difference2 - tmp) / (2 * Pow(tmp,2));
    fixed b = (x-alpha) / Pow(tau,2);
    return a - b;
}

// @Glicko2
fixed gf_DetermineSigma(structref<gs_Glicko2> self, structref<gs_Rating> rating, fixed difference, fixed variance){
    fixed phi = rating.phi;
    fixed difference2 = Pow(difference, 2);
    fixed alpha = (Log2(Pow(rating.sigma,2))/Log2(2.71828));

    fixed a;
    fixed b;
    fixed c;

    fixed k;

    fixed f_a;
    fixed f_b;
    fixed f_c;

    int count = 0;

    fixed temp_a;
    fixed temp_b;
    fixed temp_tmp;


    a = alpha;
    
    if(difference2 > Pow(phi,2) + variance){
        b = (Log2(difference2 - Pow(phi,2) - variance)/Log2(2.71828));
    } else {
        k = 1;
        while(
            f(alpha - k * SquareRoot(Pow(self.tau,2)), difference2, variance, alpha, self.tau, phi) < 0
        ){
            k += 1;
        }
        b = alpha - k * SquareRoot(Pow(self.tau, 2));
    }

    f_a = f(a, difference2, variance, alpha, self.tau, phi);
    f_b = f(b, difference2, variance, alpha, self.tau, phi);

    while(AbsF(b-a) > self.epsilon){
        c = a + (a - b) * f_a / (f_b - f_a);
        f_c = f(c, difference2, variance, alpha, self.tau, phi);
        if(f_c * f_b > 0){
            a = b;
            f_a = f_b;
        } else {
            f_a /= 2;
        }
        b = c;
        f_b = f_c;
    }

    return  Pow(Pow(2.718281,1),(a/2));
}

// @Glicko2
void gf_Rate(structref<gs_Glicko2> self, structref<gs_Rating> p_rating, structref<gs_Series> series, structref<gs_Rating> target){
    gs_Rating rating;
    gs_Rating final_rating;

    gs_Rating other_rating;

    fixed d_square_inv = 0;
    fixed variance_inv = 0;
    fixed difference = 0;

    fixed phi_star;

    int lv_enum;

    fixed impact;
    fixed expected_score;

    fixed variance;
    fixed denom;
    fixed phi;
    fixed sigma;
    fixed mu;

    gf_ScaleDown(self, rating, p_rating, DEFAULT);

    for(lv_enum = 0; lv_enum < MAX_OTHER_PLAYERS; lv_enum += 1){
        gf_ScaleDown(self, other_rating, series.ratings[lv_enum], DEFAULT);
        impact = gf_ReduceImpact(self, other_rating);
        expected_score = gf_ExpectScore(self, rating, other_rating, impact);
        variance_inv += Pow(impact, 2) * expected_score * (1-expected_score);
        difference += impact * (series.won[lv_enum]-expected_score);
        d_square_inv += (expected_score * (1 - expected_score) *
        Q_SQUARED * Pow(impact, 2));
    }

    difference /= variance_inv;
    variance = 1. / variance_inv;
    denom = (Pow(rating.phi, -2)*MULTIPLIER + d_square_inv)/MULTIPLIER;
    phi = SquareRoot(1/denom);

    sigma = gf_DetermineSigma(self, rating, difference, variance);
    
    phi_star = SquareRoot(Pow(phi,2) + Pow(sigma,2));
    phi = 1/SquareRoot(1/Pow(phi_star,2) + 1/variance);
    mu = rating.mu + Pow(phi,2) * (difference/variance);

    Rating(self, final_rating, mu, phi, sigma);
    gf_ScaleUp(self, target, final_rating, DEFAULT);
}